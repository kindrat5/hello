FROM ulsmith/debian-apache-php
MAINTAINER You <kindrat5@gmail.com>

 #Copy your files to working directory /var/www/html
 ADD ./www /var/www/html
 RUN chmod -R 0755 /var/www/html

EXPOSE 80
CMD ["/run.sh"]
